#Install settings default
file variables.tf

variable "region" {
    description = "Please Enter region"
    type        = string
    default     = "us-east-1"
}

variable "instance_type" {
    description = "Enter instance_type"
    type        = string
    default     = "t3.medium"
}

variable "allow_ports" {
    description = "Listof Port to open for server"
    type        = list
    default     = ["80", "443", "22", "8080", "9090"]
}

variable "volume_size" {
    description = "Enter instance_type"
    type        = string
    default     = 25
}