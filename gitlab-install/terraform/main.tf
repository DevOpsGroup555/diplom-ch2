provider "aws" {
  profile = "default"
  region  = var.region
}

#Search Ubuntu  20.04
data "aws_ami" "ubuntu" {
     owners      = ["099720109477"] # Ubuntu
     most_recent = true
     filter {
       name   = "name"
       values = ["ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-server-*"]  
     }     
     filter {
       name   = "virtualization-type"
       values = ["hvm"]  
    }     
}

resource "aws_instance" "ubuntu" {
    ami = data.aws_ami.ubuntu.id
    instance_type = var.instance_type
    vpc_security_group_ids = [aws_security_group.web-server.id]
    key_name        = "server2"
    root_block_device {
        volume_size = var.volume_size
        volume_type = "gp2"
    }
    tags = {
      Name = "Ubuntu"
      Owner = "Alexey"
    } 
}

#Security Group
resource "aws_security_group" "web-server" {
  name        = "Dynamic SecurityGroup"
  description = "Allow inbound traffic"

dynamic "ingress" {
  for_each = var.allow_ports
  content {
    from_port        = ingress.value
    to_port          = ingress.value
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
  }
}

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }
}


